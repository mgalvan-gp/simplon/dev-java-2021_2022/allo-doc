package proj.simplon.co.promo16.repository;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import proj.simplon.co.promo16.entity.Appointment;

public class AppointmentRepoTest {
    private AppointmentRepo appointmentRepo;

    @Before
    public void setUp() {
        appointmentRepo = new AppointmentRepo();
        appointmentRepo.getConnection();
    }

    @Test
    public void testFindAll() {
        List<Appointment> result = appointmentRepo.findAll();
        assertEquals(1, result.size());
    }

    @Test
    public void testFindById() {
        Appointment appointment = appointmentRepo.findById(1);
        assertTrue(appointment.getId() == 1);
    }

    @Test
    public void testSave() {
        Appointment appointment = new Appointment("TEST", "TEST", LocalDate.of(2022, 03, 01), LocalTime.of(16, 30, 00));
        appointmentRepo.save(appointment);
        assertTrue(appointment.getId() == 2);
    }
}
