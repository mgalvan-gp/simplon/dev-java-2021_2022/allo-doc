package proj.simplon.co.promo16.entity;

public class Medecin {
    private Integer id;
    private Speciality speciality;
    private String[] disponibilities;
    private String firstName;
    private String lastName;

    public Medecin(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public Medecin(Integer id,String firstName, String lastName) {
        this.id= id;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public Medecin() {
    }

    public Medecin(Speciality speciality, String[] disponibilities, String firstName, String lastName) {
        this.speciality = speciality;
        this.disponibilities = disponibilities;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public Medecin(Integer id, Speciality speciality, String[] disponibilities, String firstName, String lastName) {
        this.id = id;
        this.speciality = speciality;
        this.disponibilities = disponibilities;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public Speciality getSpeciality() {
        return speciality;
    }

    public void setSpeciality(Speciality speciality) {
        this.speciality = speciality;
    }

    public String[] getDisponibilities() {
        return disponibilities;
    }

    public void setDisponibilities(String[] disponibilities) {
        this.disponibilities = disponibilities;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    @Override
    public String toString() {
        return "Medecin [firstName=" + firstName + ", lastName=" + lastName + ", id=" + id + "]";
    }

}
