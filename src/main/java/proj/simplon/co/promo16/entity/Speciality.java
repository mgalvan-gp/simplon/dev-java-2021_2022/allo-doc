package proj.simplon.co.promo16.entity;

public class Speciality {
    private int speciality_id;
    private String title;

    public Speciality() {
    }

    public Speciality(int speciality_id, String title) {
        this.speciality_id = speciality_id;
        this.title = title;
    }

    public Speciality(String title) {
        this.title = title;
    }

    public int getSpeciality_id() {
        return speciality_id;
    }

    public void setSpeciality_id(int speciality_id) {
        this.speciality_id = speciality_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
