package proj.simplon.co.promo16.entity;

public class Patient {
    /* CNSS caisse Nationale de sécurite social */
    private String phoneNumber;
    private Integer id;
    private int cnss;
    private String firstName;
    private String lastName;
    private String email;
    private Profil profil;
    // private String avatar;

    public Patient(String phoneNumber, Integer id, String firstName, String lastName) {
        this.phoneNumber = phoneNumber;
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public Patient(String phoneNumber, String firstName, String lastName) {
        this.phoneNumber = phoneNumber;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public Patient(Integer id, String firstName, String lastName) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Patient() {
    }

    public Patient(int cnss, String firstName, String lastName, String email, Profil profil) {
        this.cnss = cnss;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.profil = profil;
    }

    public Patient(Integer id, int cnss, String firstName, String lastName, String email, Profil profil) {
        this.id = id;
        this.cnss = cnss;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.profil = profil;
    }

    public int getCnss() {
        return cnss;
    }

    public void setCnss(int cnss) {
        this.cnss = cnss;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Profil getProfil() {
        return profil;
    }

    public void setProfil(Profil profil) {
        this.profil = profil;
    }

    @Override
    public String toString() {
        return "Patient [firstName=" + firstName + ", id=" + id + ", lastName=" + lastName + ", phoneNumber="
                + phoneNumber + "]";
    }

    
}
