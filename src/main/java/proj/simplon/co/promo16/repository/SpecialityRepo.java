package proj.simplon.co.promo16.repository;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import proj.simplon.co.promo16.entity.Speciality;

public class SpecialityRepo implements IRepository<Speciality> {
    private Connection connection;

    public SpecialityRepo(Connection connection) {
        try {
            this.connection = DriverManager.getConnection("jdbc:mysql://simplon:1234@localhost:3306/allodocdb");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<Speciality> findAll() {
        try {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM speciality");
            ResultSet result = stmt.executeQuery();
            List<Speciality> specialityList = new ArrayList<>();
            while (result.next()) {
                Speciality speciality = new Speciality(
                    result.getInt("speciality_id"),
                    result.getString("title"));
                    specialityList.add(speciality);
            }
            return specialityList;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Speciality findById(Integer id) {
        
        return null;
    }

    @Override
    public boolean save(Speciality entity) {
        
        return false;
    }

    @Override
    public boolean update(Speciality entity) {
        
        return false;
    }

    @Override
    public boolean deleteById(Integer id) {
        
        return false;
    }
    
}
